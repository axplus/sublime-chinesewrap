# encoding: utf-8
#
# fillcommand.py
# Created by Yijie Zeng on 2013-12-2.
# Copyright (c) 2013 axplus.org. All rights reserved.
#
# 放弃Sublime Text而使用Emacs的成本太高了，算算我只不过是喜欢Emacs的中文化字
# 体、中文断行、自动fill功能，现在好了，自动fill的我已经通过插件实现了；中文化
# 断行的功能，现在看来要根治有点难度，依赖于st本身，先不动了；中文字体的功能，
# 通过制作一种新的字体可以解决
#
# 在当年，如果水平不够的情况下，就不应该去做抽象的事情，用流水账的方式写出来还
# 好维护一些
#
#
# todo:
# 1. 修正第一行和最后一行没有办法正确缩进的问题
# 2. 从分词的函数中抽离换行的逻辑
#


import unicodedata
import sublime
import sublime_plugin


PRINTABLE_CATEGORIES = set(('Lu', 'Ll', 'Lo'))
LINE_ENDINGS = {'Unix': '\n', 'Windows': '\r\n', }


class FillCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        """提取出所有带有相同前缀的行 """
        pr = RegionWithSamePrefix.find_region(self.view)

        # 分词
        words = explode_words_from_region(pr)
        # word_wrap = self.view.settings().get('word_wrap')
        wrap_width = self.view.settings().get('wrap_width') or 80

        # 自动换行
        lines = implode_words_with_wrapping(words, pr._prefix, wrap_width)
        line_endings = LINE_ENDINGS[self.view.line_endings()]
        txt = line_endings.join(lines)

        # replace
        self.view.replace(edit, sublime.Region(pr._a, pr._b), txt)


class RegionWithSamePrefix(object):
    """这个类封装了三个一起的变量：_a,_b,_prefix"""

    @staticmethod
    def find_region(view):
        return RegionWithSamePrefix(view)

    def __init__(self, view):
        self._a, self._b = 0, 0
        self._view = view
        self._prefix = ''

        self._find_region()

        print('--------_a=%d,_b=%d,_prefix="%s"' %
              (self._a, self._b, self._prefix))

    def get_chars_without_prefix(self):
        """返回这个region中的抽象、核心的内容，比如注释段中的内容，而不包含注释号"""
        prefixlen = len(self._prefix)
        for line_reg in self._view.lines(sublime.Region(self._a, self._b)):
            s = self._view.substr(line_reg)

            # 去得注释中多余的空格
            t = s[prefixlen:].strip()
            # print('t=', t)
            for c in t:
                yield c

    def _find_region(self):
        """
推导前缀：
1. 如果只有一行，通过_guess_prefix_from_one_line()
2. 如果有多行，取最大的共同前缀
"""
        reg = next((r for r in self._view.sel()))
        r, _ = self._view.rowcol(reg.begin())

        # 寻找相同前缀的行
        # 向前查找
        begin_line, prefix = self._find_line_num_next_to(
            r, -1, direction_step=-1)
        # 向后查找
        end_line, prefix = self._find_line_num_next_to(
            r, 10000, direction_step=1, ref_prefix=prefix)

        # update region
        self._a = self._view.line(self._view.text_point(begin_line, 0)).begin()
        self._b = self._view.line(self._view.text_point(end_line, 0)).end()

        self._prefix = prefix

    def _find_line_num_next_to(self, begin, end, direction_step, ref_prefix=None):
        """
从[begin,end)范围，查到前缀不同的为止

@param ref_prefix
    这个参数应该有三个状态：1. 没有；2. 空字符串；3. 字符串
"""
        prefix = ref_prefix
        for ln in range(begin, end, direction_step):
            txt_pt = self._view.text_point(ln, 0)
            line_reg = self._view.line(txt_pt)
            line_str = self._view.substr(line_reg)
            if not has_printable(line_str):
                # 空行
                break

            if ref_prefix is None:
                ref_prefix = prefix = guess_prefix_from_one_line(line_str)
            else:
                prefix = extract_prefix(line_str, prefix)

            # 如果得到的前缀是空字符串的
            if len(prefix) == 0:
                # 如果参考的不是空的
                if len(ref_prefix) > 0:
                    break
        return ln - direction_step, prefix  # previous line


def gbklen(str1):
    """用英文1个单位宽度，中文2个单位来计算字符串长度"""
    l = (2 if unicodedata.category(c) in ('Lo', 'Po') else 1
         for c in str1)
    return sum(l)


def is_printable_character(c):
    ca = unicodedata.category(c)
    return ca in PRINTABLE_CATEGORIES


def has_printable(line_str):
    for c in line_str:
        if is_printable_character(c):
            return True
    return False


def guess_prefix_from_one_line(line_str):
    prefix_chars = []
    for c in line_str:
        if is_printable_character(c):
            break
        prefix_chars.append(c)
    return ''.join(prefix_chars)


def extract_prefix(str1, str2):
    """提取出str1和str2共同的前缀"""
    buf = []
    for c in range(0, min(len(str1), len(str2))):
        if str1[c] == str2[c]:
            buf.append(str1[c])
        else:
            break
    return ''.join(buf)


def explode_words_from_region(region):
    """
这个东西根据字符和如下规则分词：
    1. 英文字符只有碰到空格分词
    2. 英文字符在标点后分词
    3. 单个中文字符后面如果没有标点可以分词
    4. 单个中文字符在标点后分词

todo:

    我觉得吧，分词就应该只做分词的事情，换行和分词不要混在一起，不然没法复用

    1. 识别中文单词，这个东西需要引入中文词库
    2. 处理多语言混排的情况
"""
    ca_last = None  # 'UNKNOWN'  # 'Lo' 'Ll' 'Lu'
    wordchars = []
    words = []
    for c in region.get_chars_without_prefix():
        ca = unicodedata.category(c)
        print('---------c="%c"---ca="%s"' % (c, ca))  # debug
        if ca_last in ('Lo', 'Po'):
            # 中文字符
            if ca in ('Po'):
                pass
            else:
                words.append(''.join(wordchars))
                wordchars = []
        elif ca_last in ('Lu', 'Ll'):
            # ascii字符
            if ca in ('Lu', 'Ll', 'Po', 'Pc'):
                pass
            else:
                words.append(''.join(wordchars))
                wordchars = []

        # buffer character, and save state
        wordchars.append(c)
        ca_last = ca

    if wordchars:
        words.append(''.join(wordchars))
    return words


def implode_words_with_wrapping(words, prefix, wrap_width):
    """do wrap"""
    lines = []
    line = [prefix]
    for word in words:
        # fixme:考虑两个英文单词相连需要加入空格的问题
        if gbklen(''.join(line)) + gbklen(word) > wrap_width:
            lines.append(''.join(line))
            line = [prefix]

        line.append(word)
    else:
        lines.append(''.join(line))
    return lines
